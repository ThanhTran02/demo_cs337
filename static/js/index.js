window.onscroll = function () {
    scrollFunction();
};

function scrollFunction() {
    var header = document.querySelector("header");
    var bannerHeight = document.querySelector(".banner").offsetHeight;
    if (document.body.scrollTop > bannerHeight || document.documentElement.scrollTop > bannerHeight-100) {
        header.style.backgroundColor = "#fff"; 
        header.style.color = "#000"; 
    } else {
        header.style.backgroundColor = "transparent";
        header.style.color = "#fff"; 
    }
}


function displayFileName() {
    var fileInput = document.getElementById('fileInput');
    var fileName = document.getElementById('fileName');
    if (fileInput.files.length > 0) {
        fileName.textContent = fileInput.files[0].name;
    } else {
        fileName.textContent = 'Choose File';
    }
}